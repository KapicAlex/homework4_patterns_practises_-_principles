import * as config from "../config";
import { activity as comment } from "../patterns/commentatorActivity";

export default class Commentator {
  constructor (io, socket, rooms) {
    this.io = io;
    this.socket = socket;
    this.rooms = rooms;
    this.startData = {};
  }

  get username() {
    return this.socket.handshake.query.username;
  }

  getStartData() {
    return this.startData;
  }

  greetUser(room, roomName) {
    if(room?.usersList.length === 1) {

      this.io.to(roomName).emit("MAKE_COMMENT", comment.greetingDefault());
      setTimeout(()=>{
        this.io.to(roomName).emit("MAKE_COMMENT", comment.joinFirstUser(this.username));
      }, 2000);

    }else{

      this.io.to(this.socket.id).emit("MAKE_COMMENT", comment.greetingDefault());
      setTimeout(()=>{
        this.io.to(roomName).emit("MAKE_COMMENT", comment.joinAnotherUsers(this.username));
      }, 2000);

    }
  }

  onReadyUser(roomName, user) {
    if(user.isReady) 
    {
      this.io.to(roomName).emit("MAKE_COMMENT", comment.onReadyUser(this.username));

    }else{

      this.io.to(roomName).emit("MAKE_COMMENT", comment.onDiscardReady(this.username));

    }
  }

  onUserQuit(roomName) {
    this.io.to(roomName).emit("MAKE_COMMENT", comment.onUserQuit(this.username));
  }

  onReadyAllUsers(roomName, room) {
    const temp =[...comment.onReadyAllUsers(room?.usersList)];
    this.startData = Object.assign({}, temp[1]);
    this.io.to(roomName).emit("MAKE_COMMENT", temp[0]);
  }

  onStartGame(roomName) {
    this.io.to(roomName).emit("MAKE_COMMENT", comment.onStartGame());
  }

  onThirtySeconds(users, roomName, gameTimer) {
    const time = config.SECONDS_FOR_GAME;
    const restOfTime = time - gameTimer;
    const usersList = users.sort((a, b) => b.progress - a.progress);

    if(gameTimer % 30 === 0 && gameTimer !== 0 && restOfTime !== 0) {
      this.io.to(roomName).emit("MAKE_COMMENT", comment.onThirtySeconds(usersList, restOfTime, gameTimer));
    }
  }

  randomComment(roomName, gameTimer) {
    const time = config.SECONDS_FOR_GAME;

    if(gameTimer % 30 !== 0
      && gameTimer !== 0
      && (time - gameTimer) !== 0
      && gameTimer % 10 === 0) {
        this.io.to(roomName).emit("MAKE_COMMENT", comment.randomComment());
      }
  }

  onFinishFirstUser(roomName, user) {
    this.io.to(roomName).emit("MAKE_COMMENT", comment.onFinishFirstUser(user));
  }

  onFinishLine(roomName) {
    this.io.to(roomName).emit("MAKE_COMMENT", comment.onFinishLine(this.username));
  }

  onFinishAnothertUsers(roomName) {
    this.io.to(roomName).emit("MAKE_COMMENT", comment.onFinishAnothertUsers(this.username));
  }

  endGame(roomName, users, time) {
    this.io.to(roomName).emit("MAKE_COMMENT", comment.endGame(users, this.startData, time));
  }

  newGame(roomName) {
    this.startData ={};
    this.io.to(roomName).emit("MAKE_COMMENT", comment.newGame());
  }
}
