import * as config from "../config";
import { texts } from "../../data"

export default function startGame (roomName, io, rooms, commentator) {
  const timerDefault = config.SECONDS_TIMER_BEFORE_START_GAME;
  let timerBefore = config.SECONDS_TIMER_BEFORE_START_GAME + 1;
  
  const interval = setInterval(() => {
    io.to(roomName).emit("TIMER_ON", [--timerBefore, timerDefault]);

    if(timerBefore === 0) {
      clearInterval(interval);

      const startData = {
        timer: config.SECONDS_FOR_GAME,
        id: Math.floor(Math.random() * texts.length),
        roomName,
      }

      io.to(roomName).emit("GAME_START", startData);

      commentator.onStartGame(roomName);

      let gameTimer = config.SECONDS_FOR_GAME;

      const gameTimerInterval = setInterval(() => {
        const room = rooms.get(roomName);

        commentator.onThirtySeconds(room?.usersList, roomName, gameTimer);

        const winners = room?.winnersList.length;
        const users = room?.usersList.length;

        if(winners === users) {
          commentator.endGame(roomName, room?.winnersList, config.SECONDS_FOR_GAME - gameTimer);
          clearInterval(gameTimerInterval);
        }

        commentator.randomComment(roomName, gameTimer);

        if(winners === 1 && users !== 1) {
          commentator.onFinishFirstUser(roomName, room?.winnersList[0]);
        }

        if(gameTimer === 0) {
          clearInterval(gameTimerInterval);
          const winnersList = room?.usersList.sort((a, b) => b.progress - a.progress);

          winnersList.forEach((user, idx) => {
            room.winnersList[idx] = user.username;
          });

          setTimeout(() => {io.to(roomName).emit("GAME_OVER", roomName)}, 7000);
          commentator.endGame(roomName, room?.winnersList, config.SECONDS_FOR_GAME);
          setTimeout(() => {commentator.newGame(roomName)}, 8000);
        }

        gameTimer--;
        io.to(roomName).emit("GAME_TIMER_UPDATE", gameTimer);
      }, 1000);
    }
  }, 1000);
}