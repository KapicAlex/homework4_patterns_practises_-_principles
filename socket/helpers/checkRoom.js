import * as config from "../config";

export function checkedRooms (rooms) {
  const maxUsers = config.MAXIMUM_USERS_FOR_ONE_ROOM;
  const filteredRooms = []

  rooms.forEach(room => {
    if(room?.usersList.length < maxUsers
      && !allReadyInRoom(room)) {
        filteredRooms.push(room)
      }
  });

  return filteredRooms;
}

export function allReadyInRoom (room) {
  const roomReady = room?.usersList.reduce((acc, user) => {
    if(user.isReady) acc++;
    return acc
  }, 0);

  return roomReady === room?.usersList.length;
}