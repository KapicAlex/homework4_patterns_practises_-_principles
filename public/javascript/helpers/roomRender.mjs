export default function roomRender (usersNum, roomName, socket) {
  const roomList = document.getElementById("rooms-list");
  const room = `<div id = "room_${roomName}" class="room-block">
                  <p class="usersNum">${usersNum} user connected</p>
                  <h2 class="room-name">Room ${roomName}</h2>
                  <button class="join-btn">Join</button>
              </div>`
  roomList.insertAdjacentHTML('beforeend', room);
  const btn = document.querySelector(`#room_${roomName} .join-btn`);
  
  btn.addEventListener("click", () => {
    socket.emit("ROOM_CONNECT", roomName)
  })
}