import { commentBackgroundImg } from "./comment.mjs"
import { commentatorImg } from "./commentator.mjs"

export default function commentatorRender() {
  const commentatorBlock = document.querySelector(".commentator-container");
  commentatorBlock.innerHTML ='';

  const comment = `<div id="comment_text" class="comment-text"></div>`;
  commentatorBlock.innerHTML = comment;
  
  commentatorBlock.insertAdjacentHTML("beforeend", commentatorImg);
  document.getElementById("comment_text").style.backgroundImage = commentBackgroundImg;
}