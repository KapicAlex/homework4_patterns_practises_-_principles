import usersInRoomRender from "./helpers/usersInRoom.mjs";
import roomRender from "./helpers/roomRender.mjs";
import fetchData from "./helpers/fetchData.mjs"
import commentatorRender from "./helpers/commentator/commentatorRender.mjs"
import makeComment from "./helpers/commentator/makeComment.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

const userIsActive = (username) => {
  window.alert(`User with name "${username}" is already registered`);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
};

const lobby = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const addRoomBtn = document.getElementById("add-room-btn");
const roomList = document.getElementById("rooms-list");
const roomInfo = document.getElementById("roomInfo");

const createRoom = () => {
  const roomName = window.prompt("Input Room Name");

  if (!roomName) {
    const roomName = window.prompt("You must enter Room Name")
  };

  socket.emit("ROOM_CREATE", roomName);
};

addRoomBtn.addEventListener("click", createRoom);

const roomIsActive = (roomName) => {
  window.alert(`Room with name "${roomName}" is already created`);
};

const quitFromRoom = (roomName, room) => {
  socket.emit("QUIT_ROOM", roomName);

  if(room?.usersList.length > 1) socket.emit("READY_ON", roomName);
}

const onReady = (roomName, username) => {
  socket.emit("READY_ON", roomName);

  const btnReady = document.getElementById("ready-btn");
  const ledIndicator = document.getElementById(`led_${username}`);
  btnReady.innerText = btnReady.innerText === "NOT READY" ? "READY" : "NOT READY";
  ledIndicator.classList.toggle("ready-status-green");
}

const connectRoomDone = ({ room }) => {
  lobby.style.display = "none";
  gamePage.style.display = "flex";
  roomInfo.innerHTML = '';

  usersInRoomRender({ room }, username, quitFromRoom, onReady);
  commentatorRender();
};

const roomsUpdate = rooms => {
  roomList.innerHTML = '';
  rooms.forEach(room => roomRender(room?.usersList.length, room.roomName, socket));
};

const quitRoomDone = rooms => {
  lobby.style.display = "block";
  gamePage.style.display = "none";
  document.getElementById("ready-btn").innerText = "READY";

  roomsUpdate(rooms);
}

const timerOn = (timers) => {

  if(timers[0] === timers[1]) {
    const btnReady = document.getElementById("ready-btn");
    const timer = document.getElementById("timer");
    const btnQuit = document.getElementById("quit-room-btn");
    btnReady.style.display = "none";
    btnQuit.style.display = "none";
    timer.style.display = "block";
  }

  timer.innerText = timers[0];
  if(timers[0] === 0) timer.style.display = "none";
}

const gameStart = ({ id, roomName }) => {
  const textBlock = document.getElementById("text-block");

  fetchData(id, textBlock, roomName, socket);
} 

const progressUpdate = ({progress, username}) => {
  const progressBar = document.querySelector(`.user-progress-${username}`);
  progressBar.style.width = progress + '%';
  
  if(progress === 100) progressBar.style.backgroundColor = 'green';
}

const gameOver = (roomName) => {
  socket.emit("NEW_GAME", roomName);

  const textBlock = document.getElementById("text-block");
  const btnReady = document.getElementById("ready-btn");
  const gameTimerBlock = document.querySelector(".game-timer");
  textBlock.innerHTML = '';
  btnReady.style.display = "block"
  btnReady.innerText = "READY";
  gameTimerBlock.style.display = "none";
}

const gameTimerUpdate = gameTimer => {
  const gameTimerBlock = document.querySelector(".game-timer");
  gameTimerBlock.style.display = gameTimer > 0 ? "block" : "none";
  gameTimerBlock.innerText = `${gameTimer} second left`;
}

socket.on("USER_IS_ACTIVE", userIsActive);
socket.on("ROOM_IS_ACTIVE", roomIsActive);
socket.on("CONNECT_ROOM_DONE", connectRoomDone);
socket.on("ROOMS_UPDATE", roomsUpdate);
socket.on("ROOM_UPDATE", connectRoomDone);
socket.on("QUIT_ROOM_DONE", quitRoomDone);
socket.on("TIMER_ON", timerOn);
socket.on("GAME_START", gameStart);
socket.on("PROGRESS_UPDATE", progressUpdate);
socket.on("GAME_OVER", gameOver);
socket.on("GAME_TIMER_UPDATE", gameTimerUpdate);
socket.on("MAKE_COMMENT", makeComment);